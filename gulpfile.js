'use strict'
const { series } = require('gulp');

var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browsersync = require('browser-sync');
	
function sassF(){
	return gulp.src('./css/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('./css'));
}

function sassWatch(){
	gulp.watch('./css/*.scss', sass);
}

function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: "./"
    },
    port: 3000
  });
  done();
}

function defaultTask(cb){
	series(browserSync, saasF);
	cb();
}

exports.saasWatch = saasWatch;
exports.browserSync = browserSync;
exports.default = defaultTask;